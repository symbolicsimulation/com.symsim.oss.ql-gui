;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.ql-gui; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(in-package :com.symsim.oss.ql-gui)

#-NIL
(defmethod print-object ((self ql-dist:dist) stream)
  (format stream "~a (~a)" (ql-dist:name self) (ql-dist:version self)))

#-NIL
(defmethod print-object ((self quicklisp-client::client-info) stream)
  (format stream "~a" (quicklisp-client::version self)))

#-NIL
(defmethod print-object ((self ql-dist:release) stream)
  (format stream "~a" (ql-dist:prefix self)))

;;
;; Hopefully reminiscent of amber warning light.
;; Colors from wikipedia page.
;;

(defconstant
  +sae-ece-amber+
  (clim-internals::make-named-color
   "sae-ece-amber"
   (/ 255 255) (/ 126 255) (/ 0 255)))

;;
;; Cache of client info so we don't keep fetching info
;; FIXME: client-info-cache reaches out at least twice - should be only once
;;

(defclass client-info-cache ()
  ((lci :reader lci :initform nil)
   (available-versions :reader available-versions :initform nil)
   (update-available-p :reader update-available-p :initform nil)))

(defmethod initialize-instance :after ((self client-info-cache) &rest args)
  (declare (ignore args))
  (with-slots (lci available-versions update-available-p) self
    (setf lci (quicklisp-client::local-client-info))
    (setf available-versions (quicklisp-client:available-client-versions))
    (setf update-available-p (client-update-available-p))))

(define-presentation-type client-info-cache ())
(define-presentation-method present (object (type client-info-cache) stream view &key)
  (present
   (lci object)
   'quicklisp-client::client-info
   :stream stream
   :single-box t)
  (if (update-available-p object)
      (with-drawing-options
       (stream :ink +red+)
       (write-string " Update available!" stream))
    (with-drawing-options
     (stream :ink +sea-green+)
     (write-string " Up to date!" stream))))

;;
;; Cache of dist so we don't keep fetching info
;; FIXME: see how many times this reaches out
;;

(defclass dist-cache ()
  ((dist :reader dist :initform nil)
   (name :reader name :initform nil)
   (version :reader version :initform nil)
   (available-versions :reader available-versions :initform nil)
   (update-available-p :reader update-available-p :initform nil)))

(defmethod initialize-instance :after ((self dist-cache) &rest args &key qldist)
  (with-slots (dist name version available-versions update-available-p) self
    (setf dist qldist)
    (setf name (ql-dist::name dist))
    (setf version (ql-dist::version dist))
    (setf available-versions (quicklisp-client:available-dist-versions name))
    (setf update-available-p (find-if
			      (lambda (v) (string> (first v) version))
			      available-versions))))

(define-presentation-type dist-cache ())
(define-presentation-method present (object (type dist-cache) stream view &key)
  (present
   (dist object)
   'ql-dist:dist
   :stream stream
   :single-box t)

  (if (ql-dist:installedp (dist object))
      (with-drawing-options
       (stream :ink +sea-green+)
       (write-string " Installed." stream))
    (with-drawing-options ; Not entirely sure how I would know about any dist that's not installed.
     (stream :ink +red+)
     (write-string " Not installed." stream)))

  (if (ql-dist:enabledp (dist object))
      (with-drawing-options
       (stream :ink +sea-green+)
       (write-string " Enabled." stream))
    (with-drawing-options
     (stream :ink +red+)
     (write-string " Not enabled." stream)))

  #+NIL (clouseau:inspect object)

  (cond ((null (available-versions object)) ; Most dists lack versions.
	 (with-drawing-options
	  (stream :ink +sae-ece-amber+)
	  (write-string " No update info!!!!" stream)))
	((update-available-p object)
	 (with-drawing-options
	  (stream :ink +red+)
	  (write-string " Update available!" stream)))
	(t
	 (with-drawing-options
	  (stream :ink +sea-green+)
	  (write-string " Up to date!" stream)))))

