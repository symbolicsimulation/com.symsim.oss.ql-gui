;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.ql-gui; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(in-package :com.symsim.oss.ql-gui)

;;
;; Command tables
;;

(make-command-table 'client-commands
		    :errorp nil
		    :menu '(("Update" :command com-update-client)))

(make-command-table 'dist-commands
		    :errorp nil
		    :menu '(("Update All Dists" :command com-update-all-dists)
			    ("Update" :command com-update-dist)
			    ("Install" :command com-install-dist)
			    ("Uninstall" :command com-uninstall-dist)
			    ("Enable" :command com-enable-dist)
			    ("Disable" :command com-disable-dist)
			    ("Clean" :command com-clean-dist)))

(make-command-table 'release-commands
		    :errorp nil
		    :menu '(("Install" :command com-install-release)
			    ("Uninstall" :command com-uninstall-release)))

(make-command-table 'ql-system-commands
		    :errorp nil
		    :menu '(("Install" :command com-install-system)
			    ("Uninstall" :command com-uninstall-system)))

(make-command-table 'quit-commands
		    :errorp nil
		    :menu '(("Program" :command com-quit-program)
			    ("Lisp" :command com-quit-lisp)))

(make-command-table 'ql-gui-menu-bar
		    :errorp nil
		    :menu '(("Client" :menu client-commands)
			    ("Dists" :menu dist-commands)
			    ("Releases" :menu release-commands)
			    ("Systems" :menu ql-system-commands)

			    ("System-Apropos" :command com-system-apropos)

			    ("Recheck Version Availability" :command com-cache-client-and-dist-versions)
			    ("Register-Local-Projects" :command com-register-local-projects)
			    ("Refresh-Frame" :command com-refresh-frame)

			    ("Quit" :menu quit-commands)))

;;
;; The application frame itself, along with its single display-function.
;;

(define-application-frame quicklisp-gui ()
  ((selected-dist :accessor selected-dist :initform nil)
   (selected-release :accessor selected-release :initform nil)
   (selected-system :accessor selected-system :initform nil)
   (selected-asdf-system :accessor selected-asdf-system :initform nil)
   ;; Cache this stuff to increase responsiveness
   (client-cache :accessor client-cache :initform nil)
   (dist-cache :accessor dist-cache :initform nil))
  (:name "A Simple GUI for Quicklisp")
  (:menu-bar ql-gui-menu-bar)
  (:pointer-documentation t)
  (:panes
   ;; Dashboard
   (dashboard
    :application
    :incremental-redisplay t
    :display-function 'display-dashboard)
   ;; Quicklisp Client info
   (client-info
    :application
    :incremental-redisplay t
    :display-function 'display-client-info)
   (client-version-list
    :application
    :incremental-redisplay t
    :display-function 'display-client-versions)
   ;; Distributions
   (dist-list
    :application
    :incremental-redisplay t
    :display-function 'display-all-dists)
   (dist-version-list
    :application
    :incremental-redisplay t
    :display-function 'display-dist-versions)
   ;; Quicklisp Releases
   (releases
    :application
    :incremental-redisplay t
    :display-function 'display-releases)
   (systems-provided-by-selected-release
    :application
    :incremental-redisplay t
    :display-function 'display-systems-provided-by-selected-release)
   ;; Quicklisp Systems
   (all-systems
    :application
    :incremental-redisplay t
    :display-function 'display-all-systems)
   (who-depends-on
    :application
    :incremental-redisplay t
    :display-function 'display-who-depends-on)
   (local-system-list
    :application
    :incremental-redisplay t
    :display-function 'display-local-systems)
   ;;
   ;; ASDF Panes.  They are redrawn as :command-loop.  This is because
   ;; I am uncertain as to when/which QL operations/commands can cause
   ;; changes in ASDF state.
   ;;
   (asdf-registered-systems
    :application
;;    :incremental-redisplay t
    :display-function 'display-asdf-registered-systems)
   (asdf-dependency-graph
    :application
;;    :incremental-redisplay t
    :display-function 'display-asdf-dependency-graph
    :height 3000
    :width 10000
    :min-width 600)
   (interactor :interactor)
   )
  (:layouts
   (default
     (vertically ()
	 (clim-tab-layout:with-tab-layout ('pane :name "TAB NAME" )
					  ("Dashboard"
					   dashboard)
					  ("Client Info"
					   (horizontally ()
					     (labelling (:label "Client Info") client-info)
					     (labelling (:label "Client Versions") client-version-list)))
					  ("Dists"
					   (horizontally ()
					     (labelling (:label "Distributions") dist-list)
					     (labelling (:label "Dist Versions") dist-version-list)
					     (labelling (:label "Releases") releases)
					     (labelling
					      (:label "Systems Provided by Selected Release")
					      systems-provided-by-selected-release)))
					  ("Quicklisp Systems"
					   (horizontally ()
					     (labelling (:label "All") all-systems)
					     (labelling (:label "Who Depends On") who-depends-on)
					     (labelling (:label "Locally Available") local-system-list)))
					  ("ASDF Systems"
					   (horizontally ()
					     (1/4 (labelling (:label "Registered Systems") asdf-registered-systems))
					     (3/4 (labelling (:label "Dependency Graph")
						    (scrolling (:width 600) asdf-dependency-graph))))))
	 (labelling (:label "Interactor") interactor)))))

			
;;
;; Ensure output goes to the interactor pane
;;

(defmethod frame-standard-output ((frame quicklisp-gui))
  (get-frame-pane frame 'interactor))

;;
;; Reach out to find updates so we don't have to fetch them all the time
;; FIXME: NEED TO DO THIS AFTER ANY DIST AND CLIENT COMMANDS!
;;

(defmethod cache-client-and-dist-versions ((frame quicklisp-gui))
  #+NIL (break "cache-client-and-dist-versions")
  (setf (client-cache frame) (make-instance 'client-info-cache))
  #+NIL (clouseau:inspect (client-cache frame))

  (setf (dist-cache frame) nil)
  (dolist (dist (ql-dist:all-dists))
    (push
     (make-instance 'dist-cache :qldist dist)
     (dist-cache frame)))
  #+NIL (clouseau:inspect (dist-cache frame)))

(defmethod initialize-instance :after ((frame quicklisp-gui) &rest args)
  (cache-client-and-dist-versions frame))

;;
;; Dashboard
;;

(defmethod display-dashboard ((frame quicklisp-gui) stream)
  (updating-output (stream)
    (updating-output (stream :cache-value (client-cache frame))
      #+NIL (clouseau:inspect :display-dashboard-client-cache)
      (write-string "Client: " stream)
      (present (client-cache frame) 'client-info-cache :stream stream)
      (terpri stream))

    (updating-output (stream :cache-value (dist-cache frame))
      #+NIL (clouseau:inspect :display-dashboard-dist-cache)
      (dolist (dc (dist-cache frame))
	(write-string "Dist: " stream)
	(present dc 'dist-cache :stream stream)
	(terpri stream)))))

;;
;; The application frame's various (mostly Quicklisp) commands.
;;

(define-quicklisp-gui-command (com-quit-program
;			       :menu "Quit Program"
			       :name "Quit Program")
  ()
  (frame-exit *application-frame*))

(define-quicklisp-gui-command (com-quit-lisp
;			       :menu "Quit Lisp"
			       :name "Quit Lisp")
  ()
  (#+SBCL sb-ext:quit
   #+CCL ccl:quit))

#|
(define-quicklisp-gui-command (com-add-to-init-file :menu "Add to Init File")
  ()
  (format *terminal-io* "com-add-to-init-file~%"))
|#

(define-quicklisp-gui-command (com-system-apropos
			:menu "System-Apropos"
			:name "System Apropos")
  ((name 'string))
  (dolist (system (quicklisp-client:system-list))
    (cond ((or (search name (ql-dist::name (ql-dist:release system)))
	       (search name (ql-dist::name system)))
	   (present system 'ql-dist:system)
	   (terpri))
	  (t nil))))

;;
;; This fixes up Quicklisp's system directory (say, after you install
;; or uninstall a local-project.
;;

(define-quicklisp-gui-command (com-register-local-projects
			:menu "Register Local Projects"
			:name "Register Local Projects")
  ()
  (ql:register-local-projects))

;;
;; This fills the frame's cache of version info.
;;

(define-quicklisp-gui-command (com-cache-client-and-dist-versions
			       :menu "Cache Client and Dist Versions"
			       :name "Cache Client and Dist Versions")
  ()
  (cache-client-and-dist-versions *application-frame*))

;;
;; This redraws the list of all quicklisp systems.
;;

(define-quicklisp-gui-command (com-refresh-frame
			:menu "Refresh Frame"
			:name "Refresh Frame")
  ()
  (redisplay-frame-panes *application-frame* :force-p t))

;;
;; Top-level.
;;

(defun run ()
  (run-frame-top-level (make-application-frame 'quicklisp-gui)))

