;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: cl-user; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(defpackage #:com.symsim.oss.ql-gui
  (:use :clim :clim-extensions :clim-lisp :quicklisp)
  (:documentation
   "A Simple GUI for Quicklisp.")
  (:export #:run))

