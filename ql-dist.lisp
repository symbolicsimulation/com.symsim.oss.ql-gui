;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.ql-gui; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(in-package :com.symsim.oss.ql-gui)

;;
;;
;;

(defun attribute-release (release stream)
  (if (ql-dist:installedp release)
      (with-drawing-options
       (stream :ink +sea-green+)
       (write-string " Installed." stream))
    (with-drawing-options
     (stream :ink +red+)
     (write-string " Not installed." stream))))

;;
;; Presentation type for only for Quicklisp's "dist" object.
;;

(define-presentation-type ql-dist:dist ())

(define-presentation-method present (object (type ql-dist:dist) stream view &key)
  (declare (ignore view))
  (print-object object stream))

;;
;; Versions are returned from quicklisp as a list of (version-string URL-string)
;;

(define-presentation-type ql-dist-version ())

(define-presentation-method present (object (type ql-dist-version) stream view &key)
  (declare (ignore view))
  (write-string (first object) stream)) ; Just show the version-string, not the URL-string

;;
;; Releases are provided by dists (of a particular version)
;;

(define-presentation-type ql-dist:release ())

(define-presentation-method present (object (type ql-dist:release) stream view &key)
  (declare (ignore view))
  #+NIL (format *terminal-io* "method present release ~a~%" object)
  (print-object object stream))

;;
;; Display methods for Quicklisp distributions
;;

(defmethod display-all-dists ((frame quicklisp-gui) stream)
  (updating-output (stream)
    (updating-output (stream :cache-value (dist-cache frame))
      (dolist (dc (dist-cache frame))
	(present dc 'dist-cache :stream stream)
	(terpri stream)))
    (setf (window-viewport-position stream) (values 0 0))))

(defmethod display-dist-versions ((frame quicklisp-gui) stream)
  (updating-output (stream)
    (updating-output (stream :cache-value (selected-dist frame))
      (when (selected-dist frame)
	(let ((dc (find-if (lambda (c) (eq (dist c) (selected-dist frame))) (dist-cache frame))))
	  #+NIL (clouseau:inspect dc)
	  (dolist (version (available-versions dc))
	    (present version 'ql-dist-version :stream stream)
	    (terpri stream)))))
    (setf (window-viewport-position stream) (values 0 0))))

(defmethod display-releases ((frame quicklisp-gui) stream)
  (updating-output (stream)
    (updating-output (stream :cache-value (selected-dist frame))
      (when (selected-dist frame)
	(dolist (release (ql-dist:provided-releases (selected-dist frame)))
	  (present release 'ql-dist:release :stream stream :single-box t)
	  (attribute-release release stream)
	  (terpri stream))))))

(defmethod display-systems-provided-by-selected-release ((frame quicklisp-gui) stream)
  (updating-output (stream)
    (updating-output (stream :cache-value (selected-release frame))
      (when (selected-release frame)
	(dolist (system (ql-dist:provided-systems (selected-release frame)))
	  (present system 'ql-dist:system :stream stream)
	  (terpri stream))))))


;;
;; Commands related to distributions
;;

(define-quicklisp-gui-command (com-update-all-dists
			:menu "Update-All-Installed-Software"
			:name "Update All Installed Software")
  ()
  (format t "Updating all dists...~%")
  (ql:update-all-dists)
  (cache-client-and-dist-versions *application-frame*)
  (format t "Done.~%"))

(define-quicklisp-gui-command (com-select-dist
			       :menu "Select Dist"
			       :name "Select Dist")
  ((dist 'ql-dist:dist
	 :gesture :select
	 :gesture :menu))
  (setf (selected-dist *application-frame*) dist)
  (setf (selected-release *application-frame*) nil)
  (setf (selected-system *application-frame*) nil))

(define-quicklisp-gui-command (com-inspect-dist
			       :menu "Inspect Dist"
			       :name "Inspect Dist")
  ((dist 'ql-dist:dist
	 :gesture :menu))
  (clouseau:inspect dist))

(define-quicklisp-gui-command (com-install-dist
			       :menu "Install Dist"
			       :name "Install Dist")
  ((name 'string))			; Should be URL
  (format t "Installing dist ~s~%" name)
  (ql-dist:install-dist name :prompt nil)
  (cache-client-and-dist-versions *application-frame*)
  (format t "Done.~%"))

(define-quicklisp-gui-command (com-uninstall-dist
			       :menu "Uninstall Dist"
			       :name "Uninstall Dist")
  ((dist 'ql-dist:dist
	 :gesture :menu))
  (format t "Uninstalling ~a~%" dist)
  (quicklisp-client:uninstall-dist (ql-dist::name dist))
  (cache-client-and-dist-versions *application-frame*)
  (format t "Done.~%"))

(define-quicklisp-gui-command (com-update-dist
			       :menu "Update Dist"
			       :name "Update Dist")
  ((dist 'ql-dist:dist
	 :gesture :menu))
  (format t "Updating ~a~%" dist)
  (ql:update-dist (ql-dist:name dist))
  (cache-client-and-dist-versions *application-frame*)
  (format t "Done.~%"))

(define-quicklisp-gui-command (com-clean-dist
			       :menu "Clean Dist"
			       :name "Clean Dist")
  ((dist 'ql-dist:dist
	 :gesture :menu))
  (format t "Cleaning ~a~%" dist)
  (ql-dist:clean dist)
  (cache-client-and-dist-versions *application-frame*) ; Not sure this is necessary
  (format t "Done.~%"))

(define-quicklisp-gui-command (com-disable-dist
			       :menu "Disable Dist"
			       :name "Disable Dist")
  ((dist 'ql-dist:dist
	 :gesture :menu))
  (format t "Disabling ~a~%" dist)
  (ql-dist:disable dist)
  (format t "Done.~%"))

(define-quicklisp-gui-command (com-enable-dist
			       :menu "Enable Dist"
			       :name "Enable Dist")
  ((dist 'ql-dist:dist
	 :gesture :menu))
  (format t "Enabling ~a~%" dist)
  (ql-dist:enable dist)
  (format t "Done.~%"))

;;
;; Commands on ql-dist-versions
;;

#+NIL
(define-quicklisp-gui-command (com-select-dist-version
			       :menu "Select Dist Version"
			       :name "Select Dist Version")
  ((dist-version 'ql-dist-version :gesture :select))
  (clouseau:inspect (list :select dist-version)))

#+NIL
(define-quicklisp-gui-command (com-describe-dist-version
			       :menu "Describe Dist Version"
			       :name "Describe Dist Version")
  ((dist-version 'ql-dist-version :gesture :describe))
  (clouseau:inspect (list :describe dist-version)))

#+NIL
(define-quicklisp-gui-command (com-edit-dist-version
			       :menu "Edit Dist Version"
			       :name "Edit Dist Version")
  ((dist-version 'ql-dist-version :gesture :edit))
  (clouseau:inspect (list :edit dist-version)))

#+NIL
(define-quicklisp-gui-command (com-inspect-dist-version
			       :menu "Inspect Dist Version"
			       :name "Inspect Dist Version")
  ((dist-version 'ql-dist-version
		 :gesture :menu))
  (clouseau:inspect dist-version))

;; There is a regression in recent McCLIM where :gesture :menu of this
;; dies as the quicklisp dist is a CONS, not a LIST, and a call to
;; LENGTH fails somewhere deep in McCLIM, and we end up in the
;; debugger.  The above gestures (:select :describe :edit, 

#-NIL
(define-quicklisp-gui-command (com-install-dist-version
                               :menu "Install Dist Version"
                               :name "Install Dist Version")
  ((dist-version 'ql-dist-version
                 :gesture :menu))
  (clouseau:inspect dist-version)
  (ql-dist:install-dist (cdr dist-version) :replace t))

;;
;; Commands on ql-dist:release
;;

(define-quicklisp-gui-command (com-select-release
			       :menu "Select Release"
			       :name "Select Release")
  ((release 'ql-dist:release
	    :gesture :select
	    :gesture :menu))
  (format t "Selecting ~a~%" release)
  (setf (selected-release *application-frame*) release)
  (setf (selected-system *application-frame*) nil)
  (format t "Done.~%"))

(define-quicklisp-gui-command (com-inspect-release
			       :menu "Inspect Release"
			       :name "Inspect Release")
  ((release 'ql-dist:release
	    :gesture :menu))
  (clouseau:inspect release))

(define-quicklisp-gui-command (com-install-release
			       :menu "Install Release"
			       :name "Install Release")
  ((release 'ql-dist:release
	    :gesture :menu))
  (format t "Installing ~a~%" release)
  (ql-dist:install release)
  (format t "Done.~%"))

(define-quicklisp-gui-command (com-uninstall-release
			       :menu "Uninstall Release"
			       :name "Uninstall Release")
  ((release 'ql-dist:release
	    :gesture :menu))
  (format t "Uninstalling ~a~%" release)
  (ql-dist:uninstall release)
  (format t "Done.~%"))
