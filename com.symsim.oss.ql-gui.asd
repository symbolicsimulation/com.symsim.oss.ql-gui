;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: cl-user; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;
;;; First   (load "com.symsim.oss.ql-gui.asd")
;;; then    (asdf:oos 'asdf:load-op :com.symsim.oss.ql-gui)
;;; finally (com.symsim.oss.ql-gui:run)
;;;

(defpackage #:com.symsim.oss.ql-gui.asd
  (:use #:cl #:asdf)
  (:export #:run))

(in-package :com.symsim.oss.ql-gui.asd)

(defsystem #:com.symsim.oss.ql-gui
    :name "com.symsim.oss.ql-gui"
    :version "0.0.4"
    :maintainer "jm@symbolic-simulation.com"
    :author "jm@symbolic-simulation.com"
    :licence "Golden Rule License"
    :description "A Simple GUI for Quicklisp and ASDF"
    :depends-on
    (#:quicklisp
     #:asdf
     #:mcclim
     #:mcclim-layouts/tab
     #:mcclim-tree-with-cross-edges
     #:clim-examples
     #:clouseau)
    :components (
		 (:file "package")
		 (:file "cache" :depends-on ("package"))
		 (:file "frame" :depends-on ("package" "cache"))
		 (:file "asdf-system" :depends-on ("package" "frame"))
		 (:file "ql-client" :depends-on ("package" "frame"))
		 (:file "ql-dist" :depends-on ("package" "frame"))
		 (:file "ql-system" :depends-on ("package" "frame"))
		 )
    :perform (load-op :after (op c)
		      (provide "com.symsim.oss.ql-gui") ; string designator
		      (format t "~&;; try (com.symsim.oss.ql-gui:run)")))

;(trace ccl:external-process-status)
;(trace ccl:run-program)
;(trace asdf/backward-interface:run-shell-command)
;(trace asdf:run-shell-command)
