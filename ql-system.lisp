;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.ql-gui; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(in-package :com.symsim.oss.ql-gui)

;;
;; Presentation type for only for Quicklisp's "system" object.
;;

(define-presentation-type ql-dist:system ())

;; Writes name of system in +sea-green+ if installed, black if not.

(define-presentation-method present (object (type ql-dist:system) stream view &key)
  (declare (ignore view))
  (let (ink)
    (cond ((ql-dist::installedp object)
	   (setf ink +sea-green+))
	  ((quicklisp-client::local-projects-searcher (ql-dist::name object))
	   (setf ink +blue+))
	  (t
	   (setf ink +black+)))
  (with-drawing-options
   (stream :ink ink)
   (write-string (ql-dist::name object) stream))))


;; Enables textual input of system names, with suggestions.

(define-presentation-method accept ((type ql-dist::system) stream view &key)
  (multiple-value-bind
   (object success str)
   (completing-from-suggestions (stream)
				(dolist (system (quicklisp-client:system-list))
				  (suggest (ql::name system) system)))
   (if success
       (values object 'ql-dist::system)
     (simple-parse-error (format nil "No system available with name ~S" str)))))

;;
;; Presentation type for uninstalled local systems (usually in ~/quicklisp/local-projects)
;;

;(define-presentation-type uninstalled-local-system () :inherit-from 'ql-dist:system)
(define-presentation-type uninstalled-local-system ())
(define-presentation-method present (object (type uninstalled-local-system) stream view &key)
  (write-string object stream))

;;
;; Display methods for Quicklisp Systems
;;

(defmethod display-who-depends-on ((frame quicklisp-gui) stream)
  (when (selected-system frame)
    (updating-output (stream)
      (dolist (depender (ql:who-depends-on (ql::name (selected-system frame))))
	#+NIL (format *terminal-io* "depender ~S~%" depender)
	#+NIL (format *terminal-io* "depender dist ~S~%" (ql-dist:find-system depender))
	#+NIL (write-string depender stream)
	(updating-output (stream :cache-value (selected-system frame))
	  (present (ql-dist:find-system depender) 'ql-dist:system :stream stream)
	  (terpri stream))))
    (setf (window-viewport-position stream) (values 0 0))))

(defmethod display-all-systems ((frame quicklisp-gui) stream)
  (updating-output (stream)
    (dolist (system (quicklisp-client:system-list))
      (updating-output (stream :cache-value system)
	(present system 'ql-dist:system :stream stream)
	(terpri stream)))
    (setf (window-viewport-position stream) (values 0 0))))

(defmethod display-local-systems ((frame quicklisp-gui) stream)
  (updating-output (stream)
    (dolist (local-system (sort (quicklisp-client:list-local-systems) #'string<))
      #+NIL (format *terminal-io* "local-system ~S~%" local-system)
      (updating-output (stream :cache-value local-system)
	(let ((remote-system (ql-dist:find-system local-system)))
	  (with-drawing-options
	   (stream :ink +blue+)
	   (present local-system 'string :stream stream))
	  (when remote-system
	    (present
	     (format nil " (shadows ~a)" remote-system)
	     'string
	     :stream stream))
	  (terpri stream))))
    (setf (window-viewport-position stream) (values 0 0))))

;;
;; Commands related to systems
;;

(define-quicklisp-gui-command (com-select-system :name "Select Quicklisp System")
  ((system 'ql-dist:system
	   :gesture :select
	   :gesture :menu))
  (setf (selected-system *application-frame*) system))

(define-quicklisp-gui-command (com-quickload-system :name "Quickload System")
  ((system 'ql-dist:system
	   :gesture :menu))
  (ql:quickload (ql::name system)))

(define-quicklisp-gui-command (com-who-depends-on-system :name "Who Depends On System")
  ((system 'ql-dist:system
	   :gesture :menu))
  (clouseau:inspect (ql:who-depends-on (ql::name system))))

(define-quicklisp-gui-command (com-install-system :name "Install System")
  ((system 'ql-dist:system
	   :gesture :menu))
  (cond ((ql-dist::installedp system)
	 (format t "Quicklisp System ")
	 (present system 'ql-dist:system)
	 (format t " already installed~%"))
	(t
	 (format t "Going to (ql:quickload ~S)~%" (ql-dist::name system))
	 (ql:quickload (ql-dist::name system)))))

(define-quicklisp-gui-command (com-uninstall-system :name "Uninstall System")
  ((system 'ql-dist:system
	   :gesture :delete
	   :gesture :menu))
  (cond ((ql-dist::installedp system)
	 (format t "Going to (ql:uninstall ~S)~%" (ql-dist::name system))
	 (ql::uninstall (ql-dist::name system))
	 (format t "Done.~%"))
	(t
	 (format t "System ")
	 (present system 'ql-dist:system)
	 (format t " not installed~%"))))

(define-quicklisp-gui-command (com-inspect-quicklisp-system :name "Inspect Quicklisp System")
  ((system 'ql-dist:system
	   :gesture :edit
	   :gesture :menu))
  (clouseau:inspect system))		; Running clouseau causes output, but I haven't looked into it.

