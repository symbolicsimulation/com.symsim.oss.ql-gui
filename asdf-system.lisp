;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.ql-gui; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(in-package :com.symsim.oss.ql-gui)

;;
;; Presentation type for ASDF system
;;

(define-presentation-type asdf:system ())

(define-presentation-method present (object (type asdf:system) stream view &key)
  (declare (ignore view))
  (write-string (asdf:component-name object) stream))

;;
;; Display methods for ASDF systems
;;

(defmethod display-asdf-registered-systems ((frame quicklisp-gui) stream)
  #+NIL (clouseau:inspect :display-asdf-registered-systems)
  (dolist (system-name (sort (asdf:registered-systems) #'string<))
    (let ((system (asdf:find-system system-name nil)))
      (cond ((null system)
	     (error (format nil "null system for system-name ~A" system-name)))
	    (t
	     (present system 'asdf:system :stream stream)
	     (terpri stream)))))
  (setf (window-viewport-position stream) (values 0 0)))

(defmethod display-asdf-dependency-graph ((frame quicklisp-gui) stream)
  #+NIL (clouseau:inspect :display-asdf-dependency-graph)
  (when (selected-asdf-system frame)
    (format-graph-from-roots
     ;; root-objects
     (list (selected-asdf-system frame))
     ;; object-printer
     (lambda (designator stream)
       #+NIL (format *terminal-io* "  DESIGNATOR ~S~%" designator)
       (let ((maybe-system (asdf/find-component:resolve-dependency-spec nil designator)))
	 (if maybe-system
	     (present maybe-system 'asdf:system :stream stream)
	   (progn
	     (present (format nil "~a" designator) 'string :stream stream)
	     #+NIL (break "STRING")))))
     ;; inferior-producer
     (lambda (designator-or-system)
       (let ((system (asdf/find-component:resolve-dependency-spec nil designator-or-system)))
	 (typecase system
	   (asdf/system:system
	    #+NIL (pprint (asdf:component-sideway-dependencies system) *terminal-io*)
	    #+asdf3 (asdf:component-sideway-dependencies system)
	    #-asdf3 (asdf::component-load-dependencies system))
	   (t
	    #+NIL (break "NO CONVERSION")
	    #+NIL (format *terminal-io* "NO CONVERSION ~s~%" designator-or-system)
	    nil))))
     :stream stream
     :orientation :vertical
     :graph-type :tree-with-cross-edges
     :merge-duplicates t
#|
     :duplicate-key
     (lambda (system)
       (format *terminal-io* "    duplicate-key 1 ~A~% " system)
       (format *terminal-io* "    duplicate-key 2 ~A~% " (asdf:component-name system))
       (if system
	   (asdf:component-name system)
	 "$$$$ SYSTEM NOT FOUND"))
     :duplicate-test
     (lambda (s1 s2)
       (format *terminal-io* "    duplicate-test ~S ~S - ~A~%" s1 s2 (string-equal s1 s2))
       (string-equal s1 s2))
|#
     :arc-drawer 
     #'(lambda (stream from-object to-object
		       x1 y1 x2 y2 
		       &rest drawing-options)
	 (declare (dynamic-extent drawing-options))
	 (declare (ignore from-object to-object))
	 (with-drawing-options
	  (stream :ink +light-grey+)
	  (apply #'draw-arrow* stream x1 y1 x2 y2 drawing-options))))))

;;
;; Commands related to ASDF
;;

(define-quicklisp-gui-command (com-inspect-ASDF-system :name "Inspect ASDF System")
;  ((system 'asdf/system:system
  ((system 'asdf:system
	   :gesture :menu))
  (clouseau:inspect system))

#+NIL
(define-quicklisp-gui-command (com-require-ASDF-system :name "Require ASDF System")
;  ((system 'asdf/system:system
  ((system 'asdf:system
	   :gesture :menu))
  (format t "Going to require system ~A~%" (asdf:component-name system))
  (asdf:require-system system)
  (format t "Done~%"))

#+NIL
(define-quicklisp-gui-command (com-load-ASDF-system :name "Load ASDF System")
;  ((system 'asdf/system:system
  ((system 'asdf:system
	   :gesture :menu))
  (format t "Going to load system ~A~%" (asdf:component-name system))
  (asdf:load-system system)
  (format t "Done~%"))

(define-quicklisp-gui-command (com-visualize-ASDF-system-dependencies :name "Visualize ASDF System Dependencies")
  ((system 'asdf:system :gesture :select))
  (format t "Going to visualize system ~A~%" (asdf:component-name system))
  (setf (selected-asdf-system *application-frame*) system)
  (format t "Done~%"))

