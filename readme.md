# com.symsim.oss.ql-gui

This is a prototype GUI for quicklisp.

 1. use git to clone this project into ~/quicklisp/local-projects
 2. (ql:quickload :com.symsim.oss.ql-gui)
 3. (com.symsim.oss.ql-gui:run)

# Screenshots

**Dashboard Tab
**

![Dashboard tab](screenshots/dashboard.png)

**Client Info Tab
**
![Client Info tab](screenshots/client-info.png)

**Dists (Distributions) Tab
**

![Dists (Distributions) Tab](screenshots/dists.png)

**Quicklisp Systems Tab
**

![Quicklisp Systems Tab](screenshots/quicklisp-systems.png)

**ASDF Systems Tab
**

![ASDF Systems Tab](screenshots/asdf-systems.png)
