;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.ql-gui; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(in-package :com.symsim.oss.ql-gui)

(defun client-update-available-p ()
  (let* ((lci (quicklisp-client::local-client-info))
	 (latest (quicklisp-client::newest-client-info lci)))
    (cond ((null latest) nil)
	  ((quicklisp-client::client-version-lessp lci latest) t)
	  (t nil))))

;;
;; Presentation types
;;

(define-presentation-type quicklisp-client::client-info ())

(define-presentation-method present (object (type quicklisp-client::client-info) stream view &key)
  (declare (ignore view))
  #+NIL (write-string (quicklisp-client::client-version) stream)
  #+NIL (format stream "local-version: ~a~%" (quicklisp-client::client-version))
  #-NIL (print-object object stream))

;;
;; Display method for Quicklisp client 
;;

(defmethod display-client-info ((frame quicklisp-gui) stream)
  (updating-output (stream)
    (updating-output (stream :cache-value (client-cache frame))
      #+NIL (clouseau:inspect :display-client-info)
      (present (lci (client-cache frame)) 'quicklisp-client::client-info :stream stream :single-box t))))

;; This should have a presentation type with command to update (downgrade????).
(defmethod display-client-versions ((frame quicklisp-gui) stream)
  (updating-output (stream)
    (updating-output (stream :cache-value (client-cache frame))
      #+NIL (clouseau:inspect :display-client-versions)
      (dolist (version (available-versions (client-cache frame)))
	(write-string (first version) stream)
	(terpri stream)))))

;;
;; Commands related to the client
;;

(define-quicklisp-gui-command (com-inspect-client-info
			       :menu "Inspect Quicklisp Client"
			       :name "Inspect Quicklisp Client")
  ((cli 'quicklisp-client::client-info
	:gesture :menu))
  (clouseau:inspect cli))

;; This is on the menubar
(define-quicklisp-gui-command (com-update-client
			:menu "Update Quicklisp Client"
			:name "Update Quicklisp Client")
  ()
  (format t "Updating client...~%")
  (ql:update-client)
  (cache-client-and-dist-versions *application-frame*)
  (format t "Done.~%"))

;; This is on the gesture menu
(define-quicklisp-gui-command (com-update-client-info
			:menu "Update Quicklisp Client Info"
			:name "Update Quicklisp Client Info")
  ((cli 'quicklisp-client::client-info
	:gesture :menu))
  (format t "Updating client...~%")
  (ql:update-client)
  (cache-client-and-dist-versions *application-frame*)
  (format t "Done.~%"))

